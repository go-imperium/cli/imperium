package run

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"

	"imperium-cli/compress"
)

var firstRun bool = true

func watchFile(path string, c chan<- bool) {
	var lastChange time.Time

	fmt.Println("-- file watcher start for:", path)
	for range time.Tick(time.Second * 2) {
		restart := false

		if stat, err := os.Stat(path); err != nil {
			cpath := filepath.Join(cwd, path)
			if sstat, eerr := os.Stat(cpath); eerr != nil {
				log.Fatal("There was an error watching file", path+":", eerr)
			} else {
				stat = sstat
				path = cpath
			}

		} else {
			if stat.ModTime().After(lastChange) {
				name := filepath.Base(path)
				if strings.Contains(name, ".") {
					idx := strings.Index(name, ".")
					name = name[:idx]
				}

				packTarget := filepath.Join(os.TempDir(), "imperium", "packing", name)
				buildDir := filepath.Join(os.TempDir(), "imperium", "build")

				fmt.Println("Unpacking IMP...")
				fmt.Println("\tName:", name)
				fmt.Println("\tPackTarget:", packTarget)
				fmt.Println("\tBuildDir:", buildDir)

				compress.Ungzip(path, packTarget)
				if err := compress.Untar(packTarget, buildDir); err != nil {
					log.Fatal("Untar Error:", err)
				}
				lastChange = time.Now()
				restart = true
			}
		}

		//send restart signal
		if restart || firstRun {
			stopProject()
			fmt.Println("Sending restart Signal")
			c <- true
			firstRun = false
		}
	}
}

func watchDir(dir string, c chan<- bool) {
	//projName := filepath.Dir(path)
	//projPath := path
	var wg sync.WaitGroup

	fmt.Println("-- directory watcher started:", dir)
	fmt.Println("Clearing:", buildPath)
	if err := os.RemoveAll(buildPath); err != nil {
		fmt.Println("Cannot delete old dir:", err)
		panic("")
	}
	for {
		fmt.Println("---- WATCHER TICK ----")
		fileCount := 0
		restart := false

		err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			defer wg.Done()
			wg.Wait()
			wg.Add(1)

			defer func() { fmt.Println("\tFile Count:", fileCount, "\n\r----------------------------------\n\r") }()
			if err != nil || info.IsDir() || strings.Contains(path, ".git") {
				//fmt.Println("SKIP:", path)
				return nil
			}

			fileCount++
			fmt.Println("-- Preparing File:", filepath.Base(path))
			rel := strings.Replace(path, cwd+string(filepath.Separator), "", -1)
			buildFile := filepath.Join(buildPath, rel)

			fmt.Println("\tRel:", rel)
			fmt.Println("\tfile:", path)
			fmt.Println("\tBuildFile:", buildFile)

			if buildStat, err := os.Stat(buildFile); err != nil || info.ModTime().After(buildStat.ModTime()) {

				//if it's a go file, flag for restart
				if strings.HasSuffix(path, ".go") && !restart {
					fmt.Println("\tFlagging for restart")
					restart = true
				}
				os.MkdirAll(filepath.Dir(buildFile), os.ModePerm)

				//var buf []byte
				//orig, err := ioutil.ReadFile(file)

				if runtime.GOOS == "windows" {

					rel = strings.ReplaceAll(rel, "/", "\\")
					buildFile = strings.ReplaceAll(buildFile, "/", "\\")

					if output, _ := exec.Command("cmd", "/C", "cd").Output(); len(output) > 0 {
						fmt.Println("CD:", string(output))
					}

					//fmt.Println("Executing command:", "cmd", "/C", "copy", "/V", path, buildFile)

					if err := exec.Command("cmd", "/C", "copy", "/V", path, buildFile).Run(); err != nil {
						fmt.Println("Command error:", err)
						panic("")
					}
				} else {
					fmt.Println("Executing Command:", "cp", path, buildFile)
					if err := exec.Command("cp", path, buildFile).Run(); err != nil {
						fmt.Println("Command error:", err)
						panic("")
					}
				}

			} else {
				fmt.Println("-- Skipping:", path)
			}
			return nil
		})

		if err != nil {
			fmt.Println("Error in file walk:", err)
			//panic("")
		}

		fmt.Println("Watch Done")
		//send restart signal
		if restart || firstRun {
			fmt.Println("-- Reloading backend code...")
			stopProject()
			c <- true
			firstRun = false
		}

	}
	fmt.Println("Watcher terminated.")

}
