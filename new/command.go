package new

import (
	"embed"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
)

var cwd string

//go:embed files/*
var fs embed.FS

var NewCmd = &cobra.Command{
	Use:   "new [project_name]",
	Short: "Create a new project",
	Long:  `Create a new Imperium project with sample code, using the project name provided.`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("New project:", args[0])
		createNewProject(args[0])
	},
}

func createNewProject(name string) {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Unable to detect working directory.")
		return
	}
	fmt.Println("Working directory:", cwd)

	name = strings.ToLower(name)
	name = strings.Replace(name, " ", "_", -1)

	projectDir := filepath.Join(cwd, name)
	fmt.Println("Project Dir:", projectDir)

	files := []string{
		"files/new/web/layouts/default.html",
		"files/new/main.go",
		"files/new/web/assets/css/style.css",
		"files/new/web/assets/js/jquery-3.4.1.js",
		"files/new/web/assets/img/laurel.png",
		"files/new/web/views/index.html",
	}

	//Create files
	for _, f := range files {
		pf, err := fs.Open(f)
		if err != nil {
			fmt.Println("Embed.FS Error:", err, "("+f+")")
			continue
		}
		defer pf.Close()

		fileName := strings.Replace(f, "files/new/", "", 1)

		//absPath := strings.Replace(f, "files/", "", 1)

		if strings.HasSuffix(fileName, "main.go") {
			fileName = filepath.Base(name) + ".go"
		}
		newFilePath := filepath.Join(projectDir, fileName)

		dirPath := filepath.Dir(newFilePath)
		fmt.Println("-- creating file:", newFilePath)
		fmt.Println("\t-- creating directory:", dirPath)
		if err := os.MkdirAll(dirPath, os.ModePerm); err != nil {
			fmt.Println("There was an error creating directory:", dirPath, "trying again.")
			if e := os.Mkdir(filepath.Dir(newFilePath), os.ModePerm); e != nil {
				fmt.Println("ERROR:", err, " _ ", e)
				fmt.Println("Unable to create the directory:", dirPath, ". Please try a different directory.")
				fmt.Println("-- rolling back changes")
				os.RemoveAll(projectDir)
				return
			}
		}
		buf, _ := ioutil.ReadAll(pf)

		fmt.Println("-- writing", newFilePath)
		ioutil.WriteFile(newFilePath, buf, os.ModePerm)
	}

	fmt.Println("-- project files: Done")

	os.Chdir(projectDir)
	if err := exec.Command("go", "mod", "init", filepath.Base(name)).Run(); err != nil {
		fmt.Println("Error initializing go mod:", err)
	}
	fmt.Println("-- go mod init: Done")

	if err := exec.Command("go", "mod", "tidy").Run(); err != nil {
		fmt.Println("Error setting up modules:", err)
	}
	fmt.Println("-- go mod tidy")

	//os.Chdir(cwd)
}
