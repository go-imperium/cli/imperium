package main

import (
	"gitlab.com/leksur/imperium"
)

//App is an instance of the Web App
var app *imperium.WebApp

func main() {
	app = imperium.New()

	app.Router.Get("/", getIndex)
	app.Start()
}

//getIndex will handle GET / requests
func getIndex(ctx *imperium.Context) imperium.Resp {
	return ctx.Render("index.html")
}
