//go:generate statik -src=./files -f -Z

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"imperium-cli/compress"
	"imperium-cli/new"
	"imperium-cli/run"

	"github.com/spf13/cobra"
)

const (
	packExt = ".imp"
	version = "0.3a"
)

var (
	cwd       string
	appDir    string
	buildPath string
	process   *exec.Cmd
	target    string
)

func init() {
	cwd, _ = os.Getwd()
	appDir, _ = filepath.Abs(filepath.Dir(os.Args[0]))
}

func main() {
	var rootCmd = &cobra.Command{Use: "imperium"}

	var version = &cobra.Command{
		Use:   "version",
		Short: "Display version",
		Long:  "Displays the program version",
		Args:  cobra.MinimumNArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Imperium CLI Tool - " + version)
		},
	}

	rootCmd.AddCommand(new.NewCmd, version, compress.CompressCmd, compress.DeompressCmd, run.GetCommand())
	rootCmd.Execute()
}
