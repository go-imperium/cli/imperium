package compress

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"archive/tar"
	enc "compress/gzip"

	"github.com/spf13/cobra"
)

const PackExt = ".imp"

var CompressCmd = &cobra.Command{
	Use:   "pack",
	Short: "Package an Imperium project",
	Long:  "Package an Imperium project for easy deployment",
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		cwd, _ := os.Getwd()
		CompressDir(cwd)
	},
}

func CompressDir(dir string) {
	packName := filepath.Base(dir)
	buildPath := filepath.Join(os.TempDir(), "imperium", "packing", packName+".tar")
	defer func() {
		os.Remove(buildPath)
	}()

	//TODO DELETE TAR and GZ files
	fmt.Println("-- packing project...", buildPath)
	if err := mkTar(dir, buildPath); err != nil {
		log.Fatal(err)
	}

	outFile, err := os.OpenFile(packName+PackExt, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal("ERR Opening outfile:", err)
	}
	fmt.Println("Creating Pack file:", packName+PackExt)
	defer outFile.Close()
	zw, cerr := enc.NewWriterLevel(outFile, enc.BestCompression)
	if cerr != nil {
		log.Fatal("Error creating new gzip writer:", cerr)
	}
	defer zw.Close()

	inFile, ierr := os.OpenFile(buildPath, os.O_RDONLY, os.ModePerm)
	if ierr != nil {
		log.Fatal("Error reading infile:", ierr)
	}
	defer inFile.Close()
	//fbytes, _ := ioutil.ReadFile(p)

	fmt.Println("-- compressing...", buildPath)

	if _, err := io.Copy(zw, inFile); err != nil {
		log.Fatal("IO COPY:", err)
	}

}

func mkTar(source, target string) error {
	fmt.Println("Tar target:", target)
	if !strings.HasSuffix(target, ".tar") {
		target += ".tar"
	}
	//filename := filepath.Base(source)

	os.MkdirAll(filepath.Dir(target), os.ModePerm)

	tarfile, err := os.Create(target)
	if err != nil {
		return err
	}
	defer tarfile.Close()

	tarball := tar.NewWriter(tarfile)
	defer tarball.Close()

	info, err := os.Stat(source)
	if err != nil {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	return filepath.Walk(source,
		func(path string, info os.FileInfo, err error) error {
			fmt.Println("Tar:", path)
			if strings.Contains(path, string(filepath.Separator)+".git") {
				fmt.Println("\tSKIPPED")
				return nil
			}
			if err != nil || strings.HasSuffix(info.Name(), PackExt) {
				return err
			}
			header, err := tar.FileInfoHeader(info, info.Name())
			if err != nil {
				return err
			}

			if baseDir != "" {
				header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
			}

			if err := tarball.WriteHeader(header); err != nil {
				return err
			}

			if info.IsDir() || strings.Contains(path, ".git/") {
				return nil
			}

			file, err := os.Open(path)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(tarball, file)
			return err
		})
}
