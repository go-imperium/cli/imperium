package run

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"time"

	"imperium-cli/compress"

	"github.com/spf13/cobra"
	"golang.org/x/net/context"
)

var (
	target    string
	buildPath string
	process   *exec.Cmd
	cwd       string
	directory string
	isFile    bool

	pid int
	ctx context.Context = context.Background()
)

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run a project",
	Long:  "While inside an Imperium project, you can use this command to start the server.",
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		c := make(chan os.Signal, 1)

		//Monitor ^C interrupt and shutdown when it happens.
		go func() {
			for range c {
				// sig is a ^C, handle it

				fmt.Println("Shutting down project...")
				os.Exit(1)
			}
		}()

		//check for params
		if len(args) > 0 {
			target = strings.ToLower(args[0])
			if stat, err := os.Stat(target); err == nil && !stat.IsDir() {
				if strings.HasSuffix(args[0], ".imp") { // We only run .imp files
					isFile = true
				}
			}
		}

		if !strings.HasSuffix(buildPath, string(filepath.Separator)) {
			buildPath = filepath.Join(buildPath, string(filepath.Separator))
		}

		if !strings.Contains(buildPath, "imperium") {
			buildPath = filepath.Join(buildPath, "imperium", "build")
		}

		buildPath = filepath.Join(buildPath, target)

		os.RemoveAll(filepath.Dir(buildPath))
		os.MkdirAll(filepath.Dir(buildPath), os.ModePerm)

		if isFile { //We're running an IMP file
			name := args[0]
			if strings.Contains(name, ".") {
				idx := strings.Index(name, ".")
				name = name[:idx]
			}
			tarTarget := filepath.Join(os.TempDir(), "imperium", "unpack", name)

			os.MkdirAll(filepath.Dir(tarTarget), os.ModePerm)
			os.MkdirAll(filepath.Dir(buildPath), os.ModePerm)

			filePath := filepath.Join(cwd, args[0])

			fmt.Println("Unpacking", args[0], "to", tarTarget)

			compress.Ungzip(filePath, tarTarget)
			if err := compress.Untar(tarTarget, buildPath); err != nil {
				log.Fatal("Untar Error:", err)
			}
			fmt.Println("Cleaning up...")
			os.Remove(tarTarget)
			//cwd = filepath.Join(projectTarget, name)
			target = filepath.Join(cwd, args[0])
			buildPath = filepath.Join(buildPath, name)
			fmt.Println("REMINDER: To stop the server, press Ctrl+C")
			var lastChange time.Time
			for {

				signal.Notify(c, os.Interrupt)

				stat, _ := os.Stat(filePath)

				if stat.ModTime().After(lastChange) {
					lastChange = stat.ModTime()
					fmt.Println("Starting project....")
					stopProject()
					runProject()
				}
				time.Sleep(time.Millisecond * 100)
			}

		} else {
			//--------------------------------------------------
			//Running from local directory
			//--------------------------------------------------

			fmt.Println("REMINDER: To stop the server, press Ctrl+C")
			for {
				signal.Notify(c, os.Interrupt)
				restart := false
				changedFiles := dirChanges(cwd)
				for targetFile, buildFile := range changedFiles {
					fmt.Println("Changed:", targetFile)

					if err := prepare(targetFile, buildFile); err != nil {
						fmt.Println("--------------------------------------------------")
						fmt.Println("Unable to prepare file:", targetFile)
						fmt.Println("\tBuild:", buildFile)
						fmt.Println("\tError:", err)
						fmt.Println("--------------------------------------------------")
					}

					if strings.HasSuffix(targetFile, ".go") { //Backend change
						restart = true
					}
				}
				if restart {
					fmt.Println("Starting project, please wait...")
					stopProject()
					runProject()
				}
			}
		}

	},
}

func init() {
	cwd, _ = os.Getwd()
	target = filepath.Base(cwd)
	buildPath = filepath.Join(os.TempDir(), "imperium", "build")

}

func GetCommand() *cobra.Command {
	runCmd.Flags().StringVarP(&buildPath, "build", "b", buildPath, "Which directory to buid and run the website")
	return runCmd
}
