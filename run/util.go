package run

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

type FileRecord struct {
	Path, BuildPath string
}

func stopProject() {
	if pid > 0 {
		fmt.Println("Shutting down running project. PID:", pid)
		if runtime.GOOS == "windows" {
			exec.Command("TASKKILL", "/T", "/F", "/PID", strconv.Itoa(process.Process.Pid)).Run()
		} else {
			exec.Command("kill", "-9", strconv.Itoa(process.Process.Pid)).Run()
		}
		//syscall.Kill(-pgid, 15)
		pid = 0
	} else {
		fmt.Println("\tNo PID!")
	}
}

func runProject() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("---------------------------------------")
			log.Println("Error while attempting to run project.")
			fmt.Println(r)
			fmt.Println("Stack:\n\r", string(debug.Stack()))
			fmt.Println("Test Output:")
			cmd := exec.Command("go", "test")
			cmd.Stderr = os.Stderr
			cmd.Stdout = os.Stdout
			cmd.Run()
			fmt.Println("(Press Ctrl+C to cancel)")
		}
	}()

	stopProject()

	//for range time.Tick(2 * time.Second) {
	fmt.Println("Starting build / run process...")
	if s, e := os.Stat(buildPath); s == nil || e != nil {
		fmt.Println("Build path does not exist!")
		panic("")
	}
	s, _ := os.Getwd()
	for s != buildPath {
		fmt.Println("Entering:", buildPath)
		os.Chdir(buildPath)
		s, _ = os.Getwd()
	}

	target := filepath.Base(buildPath) + ".go"

	fmt.Println("Current Directory:", s)
	fmt.Println("Looking for build Target:", target)
	if f, e := os.Stat(target); e != nil || f.Size() == 0 {
		fmt.Println("\tLooking for generic go file...")
		if f, e = os.Stat(filepath.Join(buildPath, "main.go")); e != nil {
			//TODO Look for all non _test.go files and run those.
			fmt.Println("Error looking for go file:", e)
			panic("")
		} else {
			target = "main.go"
		}
	}
	exeName := filepath.Base(buildPath)
	if runtime.GOOS == "windows" {
		exeName += ".exe"
	}

	var output []byte
	var err error

	fmt.Println("Running", "go", "generate")
	output, err = exec.Command("go", "generate").Output()

	if err != nil {
		fmt.Println("Error running go:generate:", err)
		fmt.Println("Attempting common fixes...")
	}

	fmt.Println("Running:", "go", "build", "-o", exeName, target)
	output, err = exec.Command("go", "build", "-o", exeName, target).Output()

	if err != nil {
		fmt.Println("ERR Building:", err)
		os.Setenv("GOPROXY", "direct")
		os.Setenv("GOPRIVATE", "gitlab.com,github.com")
		if output, err = exec.Command("go", "build", "-o", exeName, target).Output(); err != nil {
			cwd, _ := os.Getwd()
			fmt.Println("-------------------------------")
			fmt.Println("Error building:", string(output), "-", err)
			fmt.Println("EXE:", exeName)
			fmt.Println("Build Path:", buildPath)
			fmt.Println("CWD:", cwd)
			fmt.Println("Target:", target)
		} else {
			fmt.Println("Setting GOPROXY=direct and GOPRIVATE=gitlab.com,github.com was successful. Please update your env vars.")
		}
	}

	process = exec.CommandContext(ctx, "./"+exeName)

	//process.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	process.Stdout = os.Stdout
	process.Stderr = os.Stderr

	go process.Start()
	<-time.After(time.Second)
	pid = process.Process.Pid

	fmt.Println("Running process PID:", pid)
	fmt.Println("To STOP the server, press CTRL+C")

	//}
}

func dirChanges(dir string) (rtn map[string]string) {
	rtn = make(map[string]string)

	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		//fmt.Println("Checking Path:", path)
		if info.IsDir() || strings.Contains(path, ".git") {
			return nil
		}

		rel := strings.Replace(path, cwd+string(filepath.Separator), "", -1)
		buildFile := filepath.Join(buildPath, rel)

		binfo, err := os.Stat(buildFile)
		if err != nil || info.ModTime().After(binfo.ModTime()) {
			rtn[path] = buildFile
		}

		return nil
	})

	return rtn
}

func prepare(file, buildFile string) error {
	//fmt.Println("Preparing file:", file)
	//rel := strings.Replace(file, cwd+string(filepath.Separator), "", -1)
	//buildFile := filepath.Join(buildPath, rel)

	os.MkdirAll(filepath.Dir(buildFile), os.ModePerm)

	//var buf []byte
	var err error
	//orig, err := ioutil.ReadFile(file)
	//fmt.Println("\tReading:", file)
	if _, err := os.Stat(file); err != nil {
		fmt.Println("File does not exist:", file)
		panic("")
	}

	inFile, err := os.OpenFile(file, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Unable to open infile:", err)
		return err
	}

	outFile, oerr := os.Create(buildFile)
	if oerr != nil {
		fmt.Println("Unable to open outFile:", oerr)
		return oerr
	}

	_, cerr := io.Copy(outFile, inFile)
	if cerr != nil {
		fmt.Println("Unable to copy contents:", cerr)
		return cerr
	}
	inFile.Close()
	outFile.Close()

	return nil
}
