## Notice: This project is not yet ready for public use

# Imperium
The Imperium command line interface is a tool to help you get started using the Imperium web app framework. It will allow you to quickly create new projects with sample code, run, package and deploy your projects.

## Installation
```go
go get gitlab.com/go-imperium/cli/imperium
go install gitlab.com/go-imperium/cli/imperium
```
Requires: GO 1.12+

*Note: Make sure GOPATH/bin is added to your PATH environment variable*

## Usage
### Create A Project
```go
imperium new <project_name>
```
Example: `imperium new mysite`

### Run A Project
Open a terminal in the root directory of your project.
```go
imperium run
```
Using Imperium CLI to run a project also provides *hot code reloading* and will restart the project automatically if backend changes are made.

### Package A Project
Open a terminal in the root directory of your project.
```go
imperium pack
```
This will package your entire project into a single file, making deployment even easier.

### Deploy A Project
Open a terminal in the same directory as your imperium package.
```go
imperium unpack <project_name>
```
or 
```go 
imperium run <imp_file>
```
This command will unpack the imperium deployment file, build the project and place it in the target directory. Alternatively, you can run your project directly from the `.imp` file.

```