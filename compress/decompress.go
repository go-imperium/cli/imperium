package compress

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
)

var DeompressCmd = &cobra.Command{
	Use:   "unpack [imp file]",
	Short: "Unpackage an Imperium project",
	Long:  "Unpackage an Imperium project for easy deployment",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		cwd, _ := os.Getwd()
		name := args[0]
		if strings.Contains(name, ".") {
			idx := strings.Index(name, ".")
			name = name[:idx]

		}
		packTarget := filepath.Join(os.TempDir(), "imperium", "packing", name)
		//buildDir := filepath.Join(os.TempDir(), "imperium", "build")
		Ungzip(filepath.Join(cwd, args[0]), packTarget)
		if err := Untar(packTarget, cwd); err != nil {
			log.Fatal("Untar Error:", err)
		}

	},
}

func Ungzip(source, target string) error {
	reader, err := os.Open(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	archive, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer archive.Close()

	target = filepath.Join(target, archive.Name)
	fmt.Println("ungzip target:", target)
	writer, err := os.Create(target)
	if err != nil {
		return err
	}
	defer writer.Close()

	_, err = io.Copy(writer, archive)
	return err
}

func Untar(tarball, target string) error {
	reader, err := os.Open(tarball)
	if err != nil {
		return err
	}
	defer reader.Close()
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		path := filepath.Join(target, header.Name)
		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return err
			}
			continue
		}

		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(file, tarReader)
		if err != nil {
			return err
		}
	}
	return err
}
